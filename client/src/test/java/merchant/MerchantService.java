package merchant;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import shared.*;

public class MerchantService {

	WebTarget facadeServiceURL;

	public MerchantService() {
		Client client = ClientBuilder.newClient();
		facadeServiceURL = client.target("http://localhost:8081/api/merchant/");
	}

	/**
	 * @author Lasse
	 */
	public String registerMerchant(DTUPayUser merchant) throws ServersideError {

		Response response = facadeServiceURL.path("account/").request()
				.post(Entity.entity(merchant, MediaType.APPLICATION_JSON));

		if (response.getStatus() == 201) {
			return response.readEntity(String.class);
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}

	/**
	 * @author Niels
	 */
	public boolean makeTransaction(int amount, DTUPayUser merchant, Token token) throws ServersideError {
		TransactionDTO transaction = new TransactionDTO();
		transaction.setAmount(new BigDecimal(amount));
		transaction.setCreditor(merchant.getId());
		transaction.setTokenID(token.getTokenId());
		transaction.setDescription("description");
		transaction.setTimestamp(LocalDateTime.now());

		Response response = facadeServiceURL.path("transaction").request()
				.post(Entity.entity(transaction, MediaType.APPLICATION_JSON));

		if (response.getStatus() == 201) {
			return true;
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}
	/**
	 * @author Ammad
	 */
	public List<MerchantTransactionDTO> getTransactionsById(String id, String startDate, String endDate) throws ServersideError {
		var response = facadeServiceURL
						.path("report/"+id)
						.queryParam("startDate", startDate)
						.queryParam("endDate", endDate)
						.request()
						.get();
		if (response.getStatus() == 200) {
			return response.readEntity(new GenericType<ArrayList<MerchantTransactionDTO>>(){});
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}


	public void unregisterMerchant(DTUPayUser merchant) throws ServerCommunicationError {
		try {
			var response = facadeServiceURL.path("/account/" + merchant.getId()).request().delete();
			if (response.getStatus() == 200) {
				return;
			} else {
				throw new ServersideError(response.readEntity(String.class));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServerCommunicationError(e.getMessage());
		}

	}

}
