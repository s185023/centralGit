package customer;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import shared.DTUPayUser;
import shared.RequestTokenDTO;
import shared.ServerCommunicationError;
import shared.ServersideError;
import shared.Token;
import shared.TransactionDTO;

public class CustomerService {

	WebTarget facadeServiceURL;

	public CustomerService() {
		Client client = ClientBuilder.newClient();
		facadeServiceURL = client.target("http://localhost:8081/api/customer/");

	}

	/**
	 * @author Niels
	 */
	public String registerCustomer(DTUPayUser customer) throws ServersideError {

		Response response = facadeServiceURL.path("account/").request()
				.post(Entity.entity(customer, MediaType.APPLICATION_JSON));

		if (response.getStatus() == 201) {
			return response.readEntity(String.class);
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}

	/**
	 * @author Victor
	 */
	public List<Token> getTokens(DTUPayUser user, int amount) throws ServersideError {
		var request = new RequestTokenDTO();
		request.amount = amount;
		request.userId = user.getId();

		var response = facadeServiceURL.path("/tokens").request()
				.post(Entity.entity(request, MediaType.APPLICATION_JSON));
		if (response.getStatus() == 201) {
			return response.readEntity(new GenericType<ArrayList<Token>>() {
			});
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}

	/**
	 * @author Lasse
	 */
	public List<Token> getTokensById(String id) throws ServersideError {
		var response = facadeServiceURL.path("/tokens/" + id).request().get();

		if (response.getStatus() == 201) {
			return response.readEntity(new GenericType<ArrayList<Token>>() {
			});
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}

	/**
	 * @author Joakim
	 */
	public void unregisterCustomer(DTUPayUser customer) throws ServersideError, ServerCommunicationError {
		try {
			var response = facadeServiceURL.path("/account/" + customer.getId()).request().delete();
			if (response.getStatus() == 200) {
				return;
			} else {
				throw new ServersideError(response.readEntity(String.class));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServerCommunicationError(e.getMessage());
		}

	}

	/**
	 * @author Francisco
	 */
	public List<TransactionDTO> getTransactionsById(String id, String startDate, String endDate)
			throws ServersideError {
		var response = facadeServiceURL.path("report/" + id).queryParam("startDate", startDate)
				.queryParam("endDate", endDate).request().get();

		if (response.getStatus() == 200) {
			return response.readEntity(new GenericType<ArrayList<TransactionDTO>>() {
			});
		} else {
			throw new ServersideError(response.readEntity(String.class));
		}
	}

}