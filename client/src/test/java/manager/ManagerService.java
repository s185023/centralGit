package manager;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import shared.*;

/**
 * @author Ammad
 */
public class ManagerService {

	private WebTarget facadeServiceURL;

	public ManagerService() {
		Client client = ClientBuilder.newClient();
		facadeServiceURL = client.target("http://localhost:8081/api/manager/");
	}

	public List<TransactionDTO> getAllTransactions() throws ServersideError {
		Response r = facadeServiceURL.path("/report").request().get();
		if (r.getStatus() == 200) {
			return r.readEntity(new GenericType<List<TransactionDTO>>() {
			});
		} else {
			throw new ServersideError(r.readEntity(String.class));
		}
	}

}
