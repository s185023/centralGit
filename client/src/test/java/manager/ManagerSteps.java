package manager;

import static org.junit.Assert.assertEquals;

import java.util.List;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.*;
import shared.ServersideError;
import shared.TransactionDTO;

/**
 * @author Francisco
 */
public class ManagerSteps {
	ManagerService managerService = new ManagerService();
	private List<TransactionDTO> reportResult;
	private List<TransactionDTO> preTransactions;

	@Before
	public void getCurrentTransactions() throws ServersideError {
		preTransactions = managerService.getAllTransactions();
	}

	@When("the manager requests to see all transactions")
	public void theManagerRequestsToSeeAllTransactions() throws ServersideError {
		reportResult = managerService.getAllTransactions();
	}

	@Then("the manager receives {int} reports\\(s)")
	public void theManagerReceivesReportsS(int number) {
		assertEquals(number, reportResult.size() - preTransactions.size());
	}
}
