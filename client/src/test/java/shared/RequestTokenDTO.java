package shared;

/**
 * @author Lasse
 */
public class RequestTokenDTO {
    public String userId;
    public int amount;

    public RequestTokenDTO() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
