package shared;

/**
 * @author Tim
 */
public class ServersideError extends Exception {

    public ServersideError(String message) {
        super(message);
    }
}
