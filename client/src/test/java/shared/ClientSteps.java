package shared;

import dtu.ws.fastmoney.User;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import merchant.MerchantService;
import manager.*;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import customer.CustomerService;
import io.cucumber.java.en.Given;

public class ClientSteps {
    MerchantService merchantService = new MerchantService();
    BankSOAPService bank = new BankSOAPService();
    CustomerService customerService = new CustomerService();

    String error;
    String merchantBankAccountId;
    String customerBankAccountId;

    User merchant;
    User customer;
    DTUPayUser registeredCustomer;
    DTUPayUser registeredMerchant;

    List<Token> tokens = new ArrayList<>();

    boolean successful = false;

    private List<MerchantTransactionDTO> merchantReportResult;
    private String cid;
    private String mid;
    private List<TransactionDTO> reportResult;


    /**
     * @author Niels
     */
    @Given("a merchant named {string} {string} with {int} cash and cpr {string}")
    public void aMerchantNamedWithCash(String firstName, String lastName, Integer amount, String cpr) throws ServersideError {
        merchant = new User();
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);

        bank.removeAccountWithCPR(cpr);
        merchantBankAccountId = bank.registerUser(merchant, new BigDecimal(amount));

    }

    /**
     * @author Lasse
     */
    @Given("the merchant is registered with DTUPay")
    public void the_merchant_is_registered_with_dtu_pay() {
        registeredMerchant = new DTUPayUser(merchant.getFirstName(), merchant.getLastName(), merchantBankAccountId,
                false);
        try {
            mid = merchantService.registerMerchant(registeredMerchant);
            registeredMerchant.setId(mid);
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Victor
     */
    @Given("a customer named {string} {string} with {int} cash and cpr {string}")
    public void aCustomerNamedWithCash(String firstName, String lastName, Integer amount, String cpr)
            throws ServersideError {
        customer = new User();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);

        bank.removeAccountWithCPR(cpr);
        customerBankAccountId = bank.registerUser(customer, new BigDecimal(amount));
    }

    /**
     * @author Ammad
     */
    @Given("the customer is registered with DTUPay")
    public void the_customer_is_registered_with_dtu_pay() {
        registeredCustomer = new DTUPayUser(customer.getFirstName(), customer.getLastName(), customerBankAccountId,
                true);

        try {
            cid = customerService.registerCustomer(registeredCustomer);
            registeredCustomer.setId(cid);
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Francisco
     */
    @When("the customer requests {int} tokens")
    @Given("customer has {int} token available")
    public void customerHasTokenAvailable(Integer amount) {
        try {
            tokens = customerService.getTokens(registeredCustomer, amount);
        } catch (ServersideError e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Niels
     */
    @When("the customer unregister from DTUPay")
    public void theyWantToUnregister() {
        try {
            customerService.unregisterCustomer(registeredCustomer);
        } catch (Exception e) {
            System.out.println("Error msg was: " + e.getMessage());
            error = e.getMessage();
        }
    }

    /**
     * @author Tim
     */
    @When("the merchant initiates a payment for {int} cash by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) {
        try {
            successful = merchantService.makeTransaction(amount.intValue(), registeredMerchant, tokens.remove(0));
        } catch (ServersideError e) {
            error = e.getMessage();
            successful = false;
        }
    }

    /**
     * @author Joakim
     */
    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        assertTrue(successful);
    }

    /**
     * @author Niels
     */
    @Then("the balance of the customer at the bank is {int} cash")
    public void the_balance_of_the_customer_at_the_bank_is_cash(Integer balance) throws ServersideError {
        BigDecimal customerBankBalance = bank.getBalance(registeredCustomer.getBankAccountNumber());
        assertEquals(balance.intValue(), customerBankBalance.intValue());
    }

    /**
     * @author Lasse
     */
    @Then("the balance of the merchant at the bank is {int} cash")
    public void the_balance_of_the_merchant_at_the_bank_is_cash(Integer balance) throws ServersideError {
        BigDecimal merchantBankBalance = bank.getBalance(registeredMerchant.getBankAccountNumber());
        assertEquals(balance.intValue(), merchantBankBalance.intValue());
    }

    /**
     * @author Victor
     */
    @Then("the payment is not successful")
    public void thePaymentIsNotSuccessful() {
        assertFalse(successful);
    }


    /**
     * @author Ammad
     */
    @Then("the registration is successful")
    public void theRegistrationIsSuccessful() {
        assertNotNull(registeredCustomer);
    }

    /**
     * @author Francisco
     */
    @Then("the customer should have {int} tokens")
    public void theCustomerShouldHaveTokens(int numberOfTokens) {
        try {
            assertEquals(numberOfTokens, customerService.getTokensById(registeredCustomer.getId()).size());
        } catch (ServersideError e) {
            error = e.getMessage();
        }
    }


    /**
     * @author Tim
     */
    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String errorMessage) {
        assertEquals(errorMessage, error);
    }

    /**
     * @author Joakim
     */
    @Then("an error message is returned saying {string} {string}")
    public void anErrorMessageIsReturnedSaying(String firstErrorMessage, String secondErrorMessage) {
        assertEquals(firstErrorMessage + registeredCustomer.getId() + secondErrorMessage, error);
    }
    /**
     * @author Lasse
     */
    @Then("an error message is returned")
    public void anErrorMessageIsReturned() {
        assertTrue("Error not present as expected", error != null);
    }

    /**
     * @author Victor
     */
    @And("the customer with id {string} is not registered with DTUPay")
    public void theCustomerWithIdIsNotRegisteredWithDTUPay(String id) {
        registeredCustomer = new DTUPayUser(customer.getFirstName(), customer.getLastName(), customerBankAccountId,
                true);
        registeredCustomer.setId(id);
    }

    /**
     * @author Francisco
     */
    @When("the merchant requests to see all his transactions")
    public void theMerchantRequestsToSeeAllHisTransactions() throws ServersideError {
        merchantReportResult = merchantService.getTransactionsById(mid, null, null);
    }

    /**
     * @author Ammad
     */
    @Then("the merchant receives a report with {int} transaction\\(s)")
    public void theMerchantReceivesReportS(int num) {
        assertEquals(num, merchantReportResult.size());
        for(MerchantTransactionDTO t : merchantReportResult) {
            assertEquals(mid, t.getCreditor());
        }
    }

    /**
     * @author Tim
     */
    @When("the customer requests to see all his transactions")
    public void theCustomerRequestsToSeeAllHisTransactions() throws ServersideError {
    	reportResult = customerService.getTransactionsById(cid, null, null);
    }

    /**
     * @author Joakim
     */
    @Then("the customer receives a report with {int} transaction\\(s)")
    public void theCustomerReceivesAReportWithTransactionS(int number) {
        assertEquals(number, reportResult.size());
        for(TransactionDTO t : reportResult) {
            assertEquals(cid, t.getDebtor());
        }
    }
    /**
     * @author Victor
     */
    @When("the merchant requests his monthly report")
    public void theMerchantRequestsHisMonthlyReport() throws ServersideError {
    	LocalDateTime todayDate = LocalDateTime.now();
    	String startDate = todayDate.withDayOfMonth(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
    	String endDate = todayDate.withDayOfMonth(todayDate.toLocalDate().lengthOfMonth()).format(DateTimeFormatter.ISO_LOCAL_DATE);
		
        merchantReportResult = merchantService.getTransactionsById(mid, startDate, endDate);
    }

    /**
     * @author Lasse
     */
    @When("the customer requests his monthly report")
    public void theCustomerRequestsHisMonthlyReport() throws ServersideError {
    	LocalDateTime todayDate = LocalDateTime.now();
    	String startDate = todayDate.withDayOfMonth(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
    	String endDate = todayDate.withDayOfMonth(todayDate.toLocalDate().lengthOfMonth()).format(DateTimeFormatter.ISO_LOCAL_DATE);
		
    	reportResult = customerService.getTransactionsById(cid, startDate, endDate);
    }

    /**
     * @author Niels
     */
    @Then("the received transactions for the customer are all from the current month")
    public void theReceivedTransactionsForTheCustomerAreAllFromTheCurrentMonth() {
    	LocalDateTime todayDate = LocalDateTime.now();
    	LocalDateTime startDate = todayDate.withDayOfMonth(1);
    	LocalDateTime endDate = todayDate.withDayOfMonth(todayDate.toLocalDate().lengthOfMonth());
    	
        for(TransactionDTO t : reportResult) {
           assertTrue(t.getTimestamp().isAfter(startDate));
           assertTrue(t.getTimestamp().isBefore(endDate));
        }
    }
    /**
     * @author Ammad
     */
    @Then("the received transactions for the merchant are all from the current month")
    public void theReceivedTransactionsForTheMerchantAreAllFromTheCurrentMonth() {
    	LocalDateTime todayDate = LocalDateTime.now();
    	LocalDateTime startDate = todayDate.withDayOfMonth(1);
    	LocalDateTime endDate = todayDate.withDayOfMonth(todayDate.toLocalDate().lengthOfMonth());
    	
        for(MerchantTransactionDTO t : merchantReportResult) {
            assertTrue(t.getTimestamp().isAfter(startDate));
            assertTrue(t.getTimestamp().isBefore(endDate));
        }
    }

    /**
     * @author Tim
     */
    @When("the merchant unregister from DTUPay")
    public void theMerchantUnregisterFromDTUPay() {
        try {
            merchantService.unregisterMerchant(registeredMerchant);
        } catch (Exception e) {
            System.out.println("Error msg was: " + e.getMessage());
            error = e.getMessage();
        }
    }
}
