package shared;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

public class BankSOAPService {
    BankService service = (new BankServiceService()).getBankServicePort();

    /**
     * @author Tim
     */
    public String registerUser(User user, BigDecimal balance) throws ServersideError {
        try {
            return service.createAccountWithBalance(user, balance);
        } catch (BankServiceException_Exception e) {
            System.err.println("Got error: " + e.getMessage());
            e.printStackTrace();
            throw new ServersideError(e.getMessage());
        }
    }

    /**
     * @author Joakim
     */
    public void removeAccountWithCPR(String cpr) {
        try {
            var account = service.getAccountByCprNumber(cpr);
            try {
                service.retireAccount(account.getId());
            } catch (Exception e) {
                e.printStackTrace();
                assertTrue("Could not delete account", false);
            }
        } catch (Exception e) {
            // Account does not exist, ignore it
        }
    }


    /**
     * @author Joakim
     */
    public BigDecimal getBalance(String id) throws ServersideError {
        try {
            return service.getAccount(id).getBalance();
        } catch (BankServiceException_Exception e) {
            System.err.println("Got error: " + e.getMessage());
            e.printStackTrace();
            throw new ServersideError(e.getMessage());
        }
    }
}
