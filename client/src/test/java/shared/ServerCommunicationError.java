package shared;

/**
 * @author Joakim
 */
public class ServerCommunicationError extends Exception {

    private static final long serialVersionUID = 2089855051208151150L;

    public ServerCommunicationError() {

    }

    public ServerCommunicationError(String message) {
        super(message);
    }

}
