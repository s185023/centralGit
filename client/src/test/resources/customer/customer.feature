Feature: Customer behaviour

  # Author: Niels
  Scenario: Register customer to DTUPay
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    When the customer is registered with DTUPay
    Then the registration is successful

  # Author: Joakim
  Scenario: Unregister customer
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    When the customer unregister from DTUPay
    And the customer requests 1 tokens
    Then an error message is returned

  # Author: Victor
  Scenario: Get tokens
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    When the customer requests 5 tokens
    Then the customer should have 5 tokens

  # Author: Lasse
  Scenario: Cannot request too many tokens
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    When the customer requests 10 tokens
    Then an error message is returned saying "user can only have a maximum of 6 tokens"

  # Author: Tim
  Scenario: Cannot get tokens when you have more than one
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    When the customer requests 3 tokens
    Then the customer should have 3 tokens
    When the customer requests 2 tokens
    Then an error message is returned saying "user with id " " has more than 1 unused tokens left"

  # Author: Victor
  Scenario: Get more tokens
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    When the customer requests 1 tokens
    Then the customer should have 1 tokens
    When the customer requests 5 tokens
    Then the customer should have 6 tokens

  # Author: Niels
  Scenario: Non DTUPay user requests tokens
    Given a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer with id "unknown" is not registered with DTUPay
    When the customer requests 1 tokens
    Then an error message is returned saying "user with id unknown is not registered with DTUPay"

  # Author: Francisco
  Scenario: DTUPay customer wants to see all his transactions
    Given a merchant named "John" "Doe" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Jane" "Doe" with 1000 cash and cpr "101010-1619"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    When the merchant initiates a payment for 200 cash by the customer
    Then the payment is successful
    When the customer requests to see all his transactions
    Then the customer receives a report with 2 transaction(s)

  # Author: Ammad
  Scenario: DTUPay customer wants to see his monthly report
    Given a merchant named "John" "Doe" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Jane" "Doe" with 1000 cash and cpr "101010-1641"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    When the merchant initiates a payment for 200 cash by the customer
    Then the payment is successful
    When the customer requests his monthly report
    Then the customer receives a report with 2 transaction(s)
    And the received transactions for the customer are all from the current month
