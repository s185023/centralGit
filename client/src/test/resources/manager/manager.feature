Feature: Manager behaviour

  # Author: Francisco
  Scenario: Get all the transactions
    Given a merchant named "John" "Doe" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Jane" "Doe" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    When the merchant initiates a payment for 200 cash by the customer
    Then the payment is successful
    When the manager requests to see all transactions
    Then the manager receives 2 reports(s)
