Feature: Merchant behaviour

  # Author: Joakim
  Scenario: Successful payment
    Given a merchant named "Dincognito" "Cameed" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    And the balance of the merchant at the bank is 100 cash
    And the balance of the customer at the bank is 900 cash

  # Author: Tim
  Scenario: Illegal request: debt inducing payment
    Given a merchant named "Dincognito" "Cameed" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 9999 cash by the customer
    Then the payment is not successful
    And the balance of the merchant at the bank is 0 cash
    And the balance of the customer at the bank is 1000 cash

  # Author: Lasse
  Scenario: Illegal request: negative money transfer
    Given a merchant named "Dincognito" "Cameed" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for -1 cash by the customer
    Then the payment is not successful
    And the balance of the merchant at the bank is 0 cash
    And the balance of the customer at the bank is 1000 cash

  # Author: Niels
  Scenario: Illegal request: transfer after user deleted
    Given a merchant named "Dincognito" "Cameed" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the customer unregister from DTUPay
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is not successful
    And the balance of the merchant at the bank is 0 cash
    # Cannot check customer balance here since the customer is not a DTUPay user

  # Author: Ammad
  Scenario: DTUPay merchant wants to see all his transactions
    Given a merchant named "John" "Doe" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Jane" "Doe" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    When the merchant initiates a payment for 200 cash by the customer
    Then the payment is successful
    When the merchant requests to see all his transactions
    Then the merchant receives a report with 2 transaction(s)

  # Author: Francisco
  Scenario: DTUPay merchants wants to see his monthly report
    Given a merchant named "John" "Doe" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Jane" "Doe" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant initiates a payment for 100 cash by the customer
    Then the payment is successful
    When the merchant initiates a payment for 200 cash by the customer
    Then the payment is successful
    When the merchant requests his monthly report
    Then the merchant receives a report with 2 transaction(s)
    And the received transactions for the merchant are all from the current month

  # Author: Tim
  Scenario: Unregister merchant
    Given a merchant named "Dincognito" "Cameed" with 0 cash and cpr "201010-7443"
    And the merchant is registered with DTUPay
    And a customer named "Kammad" "Pameed" with 1000 cash and cpr "101010-1649"
    And the customer is registered with DTUPay
    And customer has 5 token available
    When the merchant unregister from DTUPay
    And the merchant initiates a payment for 100 cash by the customer
    Then an error message is returned