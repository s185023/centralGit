#!/bin/bash
set -e

# Build the services
pushd token-management
./build.sh
popd 

pushd rest-facade
./build.sh
popd 

pushd TransactionMicroservice
./build.sh
popd

pushd MoneyTransferService
./build.sh
popd

pushd userAccountManagement
./build.sh
popd

pushd ReportService
./build.sh
popd

# .. Add more services


# Deploy
docker image prune -f

docker-compose up -d rabbitMq
sleep 10s
docker-compose up -d token-management transaction-management moneytransferservice user_account_management reportmicroservice rest-facade
sleep 10s

pushd client
./build.sh
popd


#pushd [client]
#./deploy.sh
#sleep 20s
#./test.sh
#popd


# This is for testing the simpleDTUpay

#pushd simpleDTUPayThing
#mvn package -Dquarkus.package.type=uber-jar
# Create a new docker image if necessary.
# Restarts the container with the new image if necessary
# The server stays running.
# To terminate the server run docker-compose down in the
# code-with-quarkus direcgtory
#docker-compose up -d
