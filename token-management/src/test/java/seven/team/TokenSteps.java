package seven.team;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messagequeues.Event;
import messagequeues.EventSender;
import messagequeues.RabbitMqSender;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;

public class TokenSteps {

    String userId;
    String returnedUserId;
    int numberOfTokens;
    String error;
    TokenRepository database = new InMemoryTokenRepository();
    EventSender sender = new RabbitMqSender();
    CompletableFuture<ArrayList<Token>> result = new CompletableFuture<>();
    CompletableFuture<Boolean> mutex = new CompletableFuture<>(); // Make sure we dont run into race conditions

    TokenManager tokenManager = new TokenManager(database, sender);
    ArrayList<Token> listOfTokens;
    Event event;

    public TokenSteps() {
        tokenManager.setSender(new EventSender() {
            @Override
            public void sendEvent(Event e) throws Exception {
                event = e;
                mutex.complete(true);
            }
        });
    }

    /**
     * @author Lasse
     */
    @Given("a user with userId {string}")
    public void a_user_with_user_id(String id) {
        userId = id;
    }

    /**
     * @author Niels
     */
    @Given("the user has {int} or less unused token left")
    public void the_user_has_or_less_unused_token_left(Integer amount) {
        boolean hasLessTokens = database.getAllTokens(userId).size() <= amount;
        assertTrue(hasLessTokens);
    }

    /**
     * @author Victor
     */
    @Given("I receive the event {string}")
    public void i_receive_the_event(String eventType) {
        try {
            tokenManager.receiveEvent(new Event(eventType, new String[] {userId}));
            result.join();
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Niels
     */
    @When("the user request {int} tokens")
    public void the_user_request_tokens(Integer amount) {
        new Thread( () -> {
            try {
                result.complete(tokenManager.giveTokensToUser(userId, amount));
            } catch (Exception e) {
                error = e.getMessage();
                result.complete(new ArrayList<>());
            }
        }).start();

        mutex.join();
    }

    /**
     * @author Victor
     */
    @Then("{int} tokens is given to the user")
    public void tokens_is_given_to_the_user(Integer tokenAmount) {

        int userTokens = database.getAllTokens(userId).size();
        assertEquals(tokenAmount, userTokens);
    }

    /**
     * @author Lasse
     */
    @Given("the user has {int} unused tokens left")
    public void the_user_has_unused_tokens_left(Integer amount) {
        numberOfTokens = amount;
        try {
            ArrayList<Token> newTokens = new ArrayList<>();
            for(int i = 0; i < amount; i++) {
                Token token = new Token(userId);
                newTokens.add(token);
            }
            database.addAllTokens(newTokens);

        } catch (Exception e) {
            error = e.getMessage();
        }

    }

    /**
     * @author Victor
     */
    @Then("the token is not given to the user")
    public void the_token_is_not_given_to_the_user() {
        int userTokens = database.getAllTokens(userId).size();
        assertEquals(numberOfTokens, userTokens);
    }

    /**
     * @author Niels
     */
    @Then("an error message is returned saying {string}")
    public void an_error_message_is_returned_saying(String errorMessage) {
        assertEquals(errorMessage, error);
    }

    /**
     * @author Lasse
     */
    @Given("the user has a token with id {string}")
    public void the_user_has_a_token_with_id(String tokenId) {
        Token token = new Token(userId, tokenId);
        database.addToken(token);
    }

    /**
     * @author Victor
     */
    @When("the token with id {string} is consumed")
    public void the_token_with_id_is_consumed(String tokenId) {
        try {
            returnedUserId = tokenManager.consume(tokenId);
        } catch (InvalidTokenException e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Lasse
     */
    @Then("the userID {string} is returned")
    public void the_user_id_is_returned(String userId) {
        assertEquals(userId, returnedUserId);
    }

    /**
     * @author Niels
     */
    @When("the list of all tokens for the user with id {string} is requested")
    public void the_list_of_all_tokens_for_the_user_with_id_is_requested(String id) {
        listOfTokens = tokenManager.getTokensForUser(id);
    }

    /**
     * @author Niels
     */
    @Then("a list with {int} tokens is returned")
    public void a_list_with_tokens_is_returned(Integer amount) {
        assertEquals(amount, listOfTokens.size());
    }

    /**
     * @author Victor
     */
    @When("I receive the event {string} which contains a user with id {string} and a token with id {string}")
    public void i_receive_the_event_which_contains_a_user_with_id_and_a_token_with_id(String event, String userId, String tokenId) {
        try {
            tokenManager.receiveEvent(new Event(event, new String[] {tokenId, "1", "2", "3", userId, "5"}));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Lasse
     */
    @Then("I send the event {string}")
    public void i_send_the_event(String sendEvent) {
        assertEquals(sendEvent, event.getEventType());
    }

    /**
     * @author Lasse
     */
    @Given("I receive the irrelevant event {string}")
    public void i_receive_the_irrelevant_event(String eventType) {
        try {
            tokenManager.receiveEvent(new Event(eventType, new String[] {"0", "1", "2", "3", "4", "5"}));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Victor
     */
    @Given("I receive the event {string} with id {string}")
    public void i_receive_the_event_with_id(String eventType, String id) {
        try {
            tokenManager.receiveEvent(new Event(eventType, new String[] {id}));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Lasse
     */
    @Then("I send no events")
    public void i_send_no_events() {
        assertNull(event);
    }

    /**
     * @author Niels
     */
    @When("I receive the event {string} with token id {string}")
    public void iReceiveTheEventWithTokenId(String eventType, String tokenId) {
        try {
            tokenManager.receiveEvent(new Event(eventType, new String[] {tokenId, "1", "2", "3", "4", "5"}));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }
}


