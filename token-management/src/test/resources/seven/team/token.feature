Feature: Token

	# Author: Niels
	Scenario: A user successfully obtain a number of tokens
		Given a user with userId "uid1"
		And the user has 1 or less unused token left
		When the user request 5 tokens
		Given I receive the event "UserVerified"
		Then 5 tokens is given to the user

	# Author: Niels
	Scenario: A user is denied more tokens
		Given a user with userId "uid1"
		And the user has 4 unused tokens left
		When the user request 2 tokens
		Given I receive the event "UserVerified"
		Then the token is not given to the user
		And an error message is returned saying "user with id uid1 has more than 1 unused tokens left"

	# Author: Niels
	Scenario: A token is consumed successfully
		Given a user with userId "uid1"
		And the user has a token with id "tid1"
		When the token with id "tid1" is consumed
		Then the userID "uid1" is returned

	# Author: Niels
	Scenario: A token is not valid
		Given a user with userId "uid1"
		And the user has a token with id "tid1"
		When the token with id "tid2" is consumed
		Then an error message is returned saying "token with id: tid2 does not exist"

	# Author: Lasse
	Scenario: Get all tokens for user
		Given a user with userId "uid1"
		And the user has 4 unused tokens left
		When the list of all tokens for the user with id "uid1" is requested
		Then a list with 4 tokens is returned

	# Author: Lasse
	Scenario: User has no tokens
		Given a user with userId "uid1"
		And the user has 4 unused tokens left
		And a user with userId "uid2"
		And the user has 0 unused tokens left
		When the list of all tokens for the user with id "uid2" is requested
		Then a list with 0 tokens is returned

	# Author: Lasse
	Scenario: A user request too many tokens
		Given a user with userId "uid1"
		And the user has 1 unused tokens left
		When the user request 10 tokens
		Given I receive the event "UserVerified"
		Then the token is not given to the user
		And an error message is returned saying "user can only have a maximum of 6 tokens"

	# Author: Niels
	Scenario: A user request 6 tokens when having 0
		Given a user with userId "uid1"
		And the user has 0 unused tokens left
		When the user request 6 tokens
		Given I receive the event "UserVerified"
		Then the token is not given to the user
		And an error message is returned saying "user can only request between 1 and 5 tokens"

	# Author: Lasse
	Scenario: Non DTUPay user requests tokens
		Given a user with userId "uid1"
		When the user request 2 tokens
		Given I receive the event "UserNotVerified"
		Then the token is not given to the user
		And an error message is returned saying "user with id uid1 is not registered with DTUPay"

	# Author: Victor
	Scenario: Receive event, TokenValidation
		Given a user with userId "uid1"
		And the user has a token with id "tid1"
		When I receive the event "TokenValidation" which contains a user with id "uid1" and a token with id "tid1"
		Then I send the event "TokenValid"

	# Author: Victor
	Scenario: Receive an irrelevant event
		Given I receive the irrelevant event "PaymentSucceeded"
		Then I send no events

	# Author: Victor
	Scenario: A users tokens is deleted
		Given a user with userId "uid1"
		And the user has 4 unused tokens left
		And I receive the event "DeleteTokensForUser" with id "uid1"
		When the list of all tokens for the user with id "uid1" is requested
		Then a list with 0 tokens is returned

	# Author: Victor
	Scenario: Invalid token
		Given a user with userId "uid1"
		And the user has 4 unused tokens left
		When I receive the event "TokenValidation" with token id "none"
		Then I send the event "TokenNotValid"
