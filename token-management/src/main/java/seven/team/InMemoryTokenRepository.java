package seven.team;

import java.util.ArrayList;

public class InMemoryTokenRepository implements TokenRepository{

    private ArrayList<Token> tokens = new ArrayList<>();
    static InMemoryTokenRepository repo = null;

    /**
     * @author Victor
     */
    // Singleton instance
    public static InMemoryTokenRepository getInstance() {
        if(repo == null) {
            repo = new InMemoryTokenRepository();
        }
        return repo;
    }

    /**
     * @author Niels
     */
    @Override
    public ArrayList<Token> getAllTokens(String userId) {
        ArrayList<Token> userTokens = new ArrayList<>();

        for (Token token:tokens) {
            if(token.getUserId().equals(userId)) {
                userTokens.add(token);
            }
        }
        return userTokens;
    }
    /**
     * @author Lasse
     */
    @Override
    public Token getToken(String tokenId) {
        for (Token token:tokens) {
            if(token.getTokenId().equals(tokenId)) {
                return token;
            }
        }
        return null;
    }
    /**
     * @author Victor
     */
    @Override
    public void addToken(Token token) {
        if (!tokens.contains(token)) {
            tokens.add(token);
        }
    }
    /**
     * @author Niels
     */
    @Override
    public void addAllTokens(ArrayList<Token> tokens) {
        for (Token token:tokens) {
            addToken(token);
        }
    }
    /**
     * @author Lasse
     */
    @Override
    public void removeToken(Token token) {
        tokens.remove(token);
    }
}
