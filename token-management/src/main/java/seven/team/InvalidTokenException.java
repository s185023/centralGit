package seven.team;
/**
 * @author Lasse
 */
public class InvalidTokenException extends Exception{
    public InvalidTokenException(String errorMessage) {
        super(errorMessage);
    }
}
