package seven.team;

import java.util.UUID;
/**
 * @author Victor
 */
public class Token {

    private String userId;
    private String tokenId;

    public Token(String userId) {
        this.userId = userId;
        this.tokenId = UUID.randomUUID().toString();
    }

    public Token(String userId, String tokenId) {
        this.userId = userId;
        this.tokenId = tokenId;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getUserId() {
        return userId;
    }
}
