package seven.team;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/tokens")
public class TokenResource {
    TokenManager tokenManager = TokenManager.getInstance();
    ArrayList<Token> tokens;


    /**
     * @author Victor
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTokens(RequestTokenDTO request){
        try {
            tokens = tokenManager.giveTokensToUser(request.getUserId(), request.getAmount());
        } catch (TooManyTokensException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
        } catch (UserIsNotRegisteredWithDTUPayException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
        return Response.status(Response.Status.CREATED).entity(tokens).build();

    }
    /**
     * @author Niels
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokensForUser(@PathParam("id") String userId) {
        ArrayList<Token> userTokens = tokenManager.getTokensForUser(userId);
        return Response.status(Response.Status.OK).entity(userTokens).build();
    }

}
