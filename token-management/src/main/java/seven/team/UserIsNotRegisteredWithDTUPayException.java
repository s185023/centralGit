package seven.team;
/**
 * @author Victor
 */

public class UserIsNotRegisteredWithDTUPayException extends Exception {
    public UserIsNotRegisteredWithDTUPayException(String errorMessage) {
        super(errorMessage);
    }

}
