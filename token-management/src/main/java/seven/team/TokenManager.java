package seven.team;

import messagequeues.Event;
import messagequeues.EventReceiver;
import messagequeues.EventSender;
import messagequeues.RabbitMqSender;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;


public class TokenManager implements EventReceiver {
    TokenRepository repository;
    Token token;
    EventSender sender;
    private CompletableFuture<Boolean> result;

    static TokenManager tokenManager = null;

    /**
     * @author Lasse
     */
    // Singleton instance
    public static TokenManager getInstance() {
        if(tokenManager == null) {
            tokenManager = new TokenManager(InMemoryTokenRepository.getInstance(), new RabbitMqSender());
        }
        return tokenManager;
    }

    public TokenManager(TokenRepository repository, EventSender sender) {
        this.repository = repository;
        this.sender = sender;
    }

    public void setSender(EventSender sender) {
        this.sender = sender;
    }

    /**
     * @author Lasse
     */
    public ArrayList<Token> giveTokensToUser(String id, int amount) throws Exception {
        if(userIsRegisteredWithDTUPay(id)) {
            if(userCanHaveMoreTokens(id, amount)) {
                ArrayList<Token> listOfTokens = requestTokens(id, amount);
                repository.addAllTokens(listOfTokens);
                return listOfTokens;
            } else {
                throw new TooManyTokensException("user with id " + id + " has more than 1 unused tokens left");
            }
        } else {
            throw new UserIsNotRegisteredWithDTUPayException("user with id " + id + " is not registered with DTUPay");
        }
    }
    /**
     * @author Victor
     */
    private boolean userIsRegisteredWithDTUPay(String id) throws Exception {
        Event event = new Event("VerifyUser", new String[] {id});
        this.result = new CompletableFuture<>();
        sender.sendEvent(event);
        return result.join();
    }

    /**
     * @author Victor
     */
    public ArrayList<Token> requestTokens(String id, int amount) {
        ArrayList<Token> newTokens = new ArrayList<>();
        for(int i = 0; i < amount; i++) {
            Token token = new Token(id);
            newTokens.add(token);
        }
        return newTokens;
    }
    /**
     * @author Niels
     */
    private boolean userCanHaveMoreTokens(String id, int amount) throws TooManyTokensException {
        int numberOfTokens = repository.getAllTokens(id).size();


        if(numberOfTokens + amount > 6) {
            throw new TooManyTokensException("user can only have a maximum of 6 tokens");
        }

        if(amount < 1 || amount > 5) {
            throw new TooManyTokensException("user can only request between 1 and 5 tokens");
        }

        return numberOfTokens <= 1;
    }
    /**
     * @author Victor
     */
    public ArrayList<Token> getTokensForUser(String userId) {
        return repository.getAllTokens(userId);

    }

    /**
     * @author Niels
     */
    public String consume(String tokenId) throws InvalidTokenException {
        token = repository.getToken(tokenId);

        if(token == null) {
            throw new InvalidTokenException("token with id: " + tokenId + " does not exist");
        }

        repository.removeToken(token);

        return token.getUserId();

    }

    /**
     * @author Lasse
     */
    public void deleteTokensForUser(String userId) {
        for (Token t : repository.getAllTokens(userId)) {
            repository.removeToken(t);
        }
    }

    /**
     * @author Victor
     */
    @Override
    public void receiveEvent(Event event) throws Exception {
        switch (event.getEventType()) {
            case "TokenValidation":
                String[] transactionParameters = event.getArguments();

                try {
                    String userId = consume(transactionParameters[0]);
                    transactionParameters[4] = userId;
                    sender.sendEvent(new Event("TokenValid", transactionParameters));

                } catch (InvalidTokenException e) {
                    sender.sendEvent(new Event("TokenNotValid", transactionParameters));
                }
                break;
            case "UserVerified":
                this.result.complete(true);
                break;
            case "UserNotVerified":
                this.result.complete(false);
                break;
            case "DeleteTokensForUser":
                deleteTokensForUser(event.getArguments()[0]);
                break;
        }
    }
}
