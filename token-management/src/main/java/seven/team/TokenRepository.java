package seven.team;

import java.util.ArrayList;
/**
 * @author Lasse
 */
public interface TokenRepository {

    ArrayList<Token> getAllTokens(String userId);

    Token getToken(String tokenId);

    void addToken(Token token);

    void addAllTokens(ArrayList<Token> tokens);

    void removeToken(Token token);

}
