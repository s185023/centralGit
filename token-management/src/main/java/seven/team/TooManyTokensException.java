package seven.team;
/**
 * @author Niels
 */
public class TooManyTokensException extends Exception{
    public TooManyTokensException(String errorMessage) {
        super(errorMessage);
    }

}
