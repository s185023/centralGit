#!/bin/bash
set -e
mvn clean package
docker-compose build user_account_management
