Feature: Account

  # Author: Joakim
  Scenario: Register user successfully
    Given a new user with bank account number "bid1"
    When the user registers with DTUPay
    Then the user is registered with DTUPay

  # Author: Tim
  Scenario: User is removed successfully
    Given a new user with bank account number "bid1"
    And the user registers with DTUPay
    And the user is registered with DTUPay
    When the user deregisters from DTUPay
    Then the user is no longer registered with DTUPay

  # Author: Joakim
  Scenario: Receiving VerifyUser, User is verified
    Given a new user with bank account number "bid1"
    And the user registers with DTUPay
    And the user is registered with DTUPay
    When I receive the event "VerifyUser"
    Then I send the event "UserVerified"

  # Author: Joakim
  Scenario: Receiving VerifyUser, User is not verified
    Given a new user with bank account number "bid1"
    And the user registers with DTUPay
    And the user is registered with DTUPay
    When I receive the event "VerifyUser" with wrong id
    Then I send the event "UserNotVerified"

  # Author: Tim
  Scenario: Receiving GetBankAccountNumbers
    Given a new user with bank account number "bid1"
    And the user registers with DTUPay
    And a new merchant with bank account number "bid2"
    And the merchant registers with DTUPay
    When I receive the event "GetBankAccountNumbers"
    Then I send the event "BankAccountNumbers"

  # Author: Tim
  Scenario: Receive an irrelevant event
    Given I receive the irrelevant event "TokenValidation"
    Then I send no events

  # Author: Tim
  Scenario: Receiving GetBankAccountNumbers: Merchant not a DTUPay user
    Given a new user with bank account number "bid1"
    And the user registers with DTUPay
    And a new merchant with bank account number "bid2"
    When I receive the event "GetBankAccountNumbers"
    Then I send the event "MerchantNotRegistered"


  # Author: Joakim
  Scenario: Remove unknown user
    Given a new user with bank account number "bid1"
    When the user deregisters from DTUPay
    Then the user is no longer registered with DTUPay

