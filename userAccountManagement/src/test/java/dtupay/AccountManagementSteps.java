package dtupay;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messagequeues.Event;
import messagequeues.EventSender;
import messagequeues.RabbitMqSender;

import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;

public class AccountManagementSteps {

    DTUPayUser payUser, merchant;
    InMemoryDTUPayUserRepository repository = new InMemoryDTUPayUserRepository();
    EventSender sender = new RabbitMqSender();
    String error;
    Event event;
    String userId, merchantId;

    DTUPayUserManager manager = new DTUPayUserManager(repository, sender);
    CompletableFuture<Boolean> mutex = new CompletableFuture<>(); // Make sure we dont run into race conditions

    public AccountManagementSteps() {
        manager.setSender(new EventSender() {
            @Override
            public void sendEvent(Event e) throws Exception {
                event = e;
                mutex.complete(true);
            }
        });
    }

    /**
     * @author Joakim
     */
    @Given("a new user with bank account number {string}")
    public void a_user_with_id_and_bank_account_number(String bankAccount) {
        payUser = new DTUPayUser();
        payUser.setBankAccountNumber(bankAccount);
    }

    /**
     * @author Joakim
     */
    @When("the user registers with DTUPay")
    public void the_user_registers_with_dtu_pay() {
        userId = manager.registerUser(payUser);
    }

    /**
     * @author Tim
     */
    @Then("the user is registered with DTUPay")
    public void the_user_is_registered_with_dtu_pay() {
        assertTrue(manager.userIsRegistered(payUser));
        assertTrue(manager.userIsRegisteredById(userId));
    }

    /**
     * @author Tim
     */
    @When("the user deregisters from DTUPay")
    public void theUserDeregistersFromDTUPay() {
        try {
            manager.removeUserById(userId);
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Joakim
     */
    @Then("the user is no longer registered with DTUPay")
    public void theUserIsNoLongerRegisteredWithDTUPay$() {
        assertFalse(manager.userIsRegistered(payUser));
        assertFalse(manager.userIsRegisteredById(userId));
    }

    /**
     * @author Joakim
     */
    @When("I receive the event {string}")
    public void i_receive_the_event(String eventType) {
        if (eventType.equals("VerifyUser")) {
            try {
                manager.receiveEvent(new Event(eventType, new String[] { userId }));
            } catch (Exception e) {
                error = e.getMessage();
            }
        } else if (eventType.equals("GetBankAccountNumbers")) {
            try {
                manager.receiveEvent(
                        new Event(eventType, new String[] { "0", "1", merchantId, "3", userId, "5", "6", "7" }));
            } catch (Exception e) {
                error = e.getMessage();
            }
        }
    }

    /**
     * @author Tim
     */
    @Then("I send the event {string}")
    public void i_send_the_event(String eventType) {
        assertEquals(eventType, event.getEventType());
    }

    /**
     * @author Tim
     */
    @When("I receive the event {string} with wrong id")
    public void i_receive_the_event_with_wrong_id(String eventType) {
        try {
            manager.receiveEvent(new Event(eventType, new String[] { "wrong id" }));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Tim
     */
    @Given("a new merchant with bank account number {string}")
    public void a_new_merchant_with_bank_account_number(String bankAccount) {
        merchant = new DTUPayUser();
        merchant.setBankAccountNumber(bankAccount);
    }

    /**
     * @author Joakim
     */
    @Given("the merchant registers with DTUPay")
    public void the_merchant_registers_with_dtu_pay() {
        merchantId = manager.registerUser(merchant);
    }

    /**
     * @author Joakim
     */
    @Given("I receive the irrelevant event {string}")
    public void i_receive_the_irrelevant_event(String eventType) {
        try {
            manager.receiveEvent(new Event(eventType, new String[] { "0", "1", "2", "3", "4", "5" }));
        } catch (Exception e) {
            error = e.getMessage();
        }
    }

    /**
     * @author Tim
     */
    @Then("I send no events")
    public void i_send_no_events() {
        assertNull(event);
    }

}
