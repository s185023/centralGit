package dtupay;

import javax.ws.rs.*;

import messagequeues.RabbitMqSender;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/account")
public class DTUPayUserResource {

    DTUPayUserManager dtuPayUserManager = new DTUPayUserManager(InMemoryDTUPayUserRepository.getInstance(),
            new RabbitMqSender());

    /**
     * @author Joakim
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(DTUPayUser user) {
        if (dtuPayUserManager.userIsRegistered(user)) {
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }
        String newDTUPayUserId = dtuPayUserManager.registerUser(user);
        return Response.status(Response.Status.CREATED).entity(newDTUPayUserId).build();
    }

    /**
     * @author Tim
     */
    @DELETE
    @Path("/{id}")
    public Response deleteAccountById(@PathParam("id") String id) {
        try {
            dtuPayUserManager.removeUserById(id);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }
    }

}
