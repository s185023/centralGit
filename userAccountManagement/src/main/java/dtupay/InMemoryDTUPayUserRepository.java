package dtupay;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tim
 */
public class InMemoryDTUPayUserRepository implements DTUPayUserRepository {

    static InMemoryDTUPayUserRepository repo = null;
    private Map<String, DTUPayUser> payUsers = new HashMap<>();

    // Singleton instance
    public static InMemoryDTUPayUserRepository getInstance() {
        if (repo == null) {
            repo = new InMemoryDTUPayUserRepository();
        }
        return repo;
    }

    @Override
    public DTUPayUser getPayUser(String id) {
        return payUsers.get(id);
    }

    @Override
    public void addPayUser(String id, DTUPayUser user) {
        payUsers.put(id, user);
    }

    @Override
    public void removePayUser(String id) {
        payUsers.remove(id);
    }

    public boolean contains(DTUPayUser user) {
        for (DTUPayUser payUser : payUsers.values()) {
            if (payUser.getBankAccountNumber().equals(user.getBankAccountNumber())) {
                return true;
            }
        }
        return false;
    }
}
