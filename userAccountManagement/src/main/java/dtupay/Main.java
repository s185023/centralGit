package dtupay;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messagequeues.EventSender;
import messagequeues.RabbitMqListener;
import messagequeues.RabbitMqSender;

/**
 * @author Joakim
 */
@QuarkusMain
public class Main {
    public static void main(String... args) {
        Quarkus.run(MyApp.class, args);
    }

    public static class MyApp implements QuarkusApplication {

        @Override
        public int run(String... args) throws Exception {
            startUp();
            Quarkus.waitForExit();
            return 0;
        }

        private void startUp() throws Exception {
            EventSender sender = new RabbitMqSender();
            DTUPayUserManager service = new DTUPayUserManager(InMemoryDTUPayUserRepository.getInstance(), sender);
            new RabbitMqListener(service).listen();
        }

    }
}