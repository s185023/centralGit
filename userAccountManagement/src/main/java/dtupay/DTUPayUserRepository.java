package dtupay;

/**
 * @author Joakim
 */
public interface DTUPayUserRepository {

    DTUPayUser getPayUser(String id);

    void addPayUser(String id, DTUPayUser user);

    void removePayUser(String id);

    boolean contains(DTUPayUser user);

}
