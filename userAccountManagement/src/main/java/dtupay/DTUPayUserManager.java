package dtupay;

import messagequeues.Event;
import messagequeues.EventReceiver;
import messagequeues.EventSender;

import java.util.UUID;

/**
 * @author Joakim
 */
public class DTUPayUserManager implements EventReceiver {

    DTUPayUserRepository repository;
    EventSender sender;

    public DTUPayUserManager(DTUPayUserRepository repository, EventSender sender) {
        this.repository = repository;
        this.sender = sender;
    }

    public void setSender(EventSender sender) {
        this.sender = sender;
    }

    public boolean userIsRegistered(DTUPayUser user) {
        return repository.contains(user);
    }

    public boolean userIsRegisteredById(String id) {
        return repository.getPayUser(id) != null;
    }

    public String registerUser(DTUPayUser user) {
        String newDTUPayUserId = UUID.randomUUID().toString();
        user.setId(newDTUPayUserId);
        repository.addPayUser(newDTUPayUserId, user);
        return newDTUPayUserId;
    }

    public void removeUserById(String userId) throws Exception {
        repository.removePayUser(userId);
        sender.sendEvent(new Event("DeleteTokensForUser", new String[] { userId }));
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals("VerifyUser")) {

            String[] userVerificationParameters = event.getArguments();

            if (userIsRegisteredById(userVerificationParameters[0])) {
                sender.sendEvent(new Event("UserVerified", userVerificationParameters));
            } else {
                sender.sendEvent(new Event("UserNotVerified", userVerificationParameters));
            }
        } else if (event.getEventType().equals("GetBankAccountNumbers")) {
            String[] transactionParameters = event.getArguments();

            // Customer
            transactionParameters[6] = repository.getPayUser(transactionParameters[4]).getBankAccountNumber();
            // Merchant
            DTUPayUser merchant = repository.getPayUser(transactionParameters[2]);
            if(merchant != null) {
                transactionParameters[7] = merchant.getBankAccountNumber();
                sender.sendEvent(new Event("BankAccountNumbers", transactionParameters));
            } else {
                sender.sendEvent(new Event("MerchantNotRegistered", new String[] { transactionParameters[2] }));
            }


        }
    }
}
