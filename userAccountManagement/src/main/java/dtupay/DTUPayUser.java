package dtupay;

/**
 * @author Tim
 */
public class DTUPayUser {
    private String id;
    private String bankAccountNumber;

    public DTUPayUser() {
    }

    public DTUPayUser(String id, String bankAccountNumber) {
        this.id = id;
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }
}
