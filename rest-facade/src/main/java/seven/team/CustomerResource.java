package seven.team;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/customer/")
public class CustomerResource {

    Client client = ClientBuilder.newClient();
    WebTarget tokenManagementURL = client.target("http://token-management:8082/");
    WebTarget accountServiceURL = client.target("http://user_account_management:8084/");
    WebTarget reportServiceURL = client.target("http://reportmicroservice:8086/");

    /**
     * @author Victor
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("tokens")
    public Response createTokens(RequestTokenDTO request){
        return tokenManagementURL.path("tokens/").request().post(Entity.entity(request, MediaType.APPLICATION_JSON));
    }

    /**
     * @author Ammad
     */
    @GET
    @Path("tokens/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokensById(@PathParam("id") String userId){
        return tokenManagementURL.path("tokens/" + userId).request().get();
    }

    /**
     * @author Niels
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("account")
    public Response register(DTUPayUser user) {
        return accountServiceURL.path("account/").request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    /**
     * @author Lasse
     */
    @DELETE
    @Path("account/{id}")
    public Response deleteAccountById(@PathParam("id") String id) {
        return accountServiceURL.path("account/" + id).request().delete();
    }

    /**
     * @author Francisco
     */
    @GET
    @Path("report/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerTransactions(@PathParam("id") String userId, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
        return reportServiceURL.path("report/customer/" + userId).queryParam("startDate", startDate).queryParam("endDate", endDate).request().get();
    }

}
