package seven.team;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/manager/")
public class ManagerResource {

    Client client = ClientBuilder.newClient();
    WebTarget reportServiceURL = client.target("http://reportmicroservice:8086/");

    /**
     * @author Ammad
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("report")
    public Response getAllTransactions() {
        return reportServiceURL.path("/report").request().get();
    }

}
