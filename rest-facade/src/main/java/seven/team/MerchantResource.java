package seven.team;

import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/merchant/")
public class MerchantResource {

    Client client = ClientBuilder.newClient();
    WebTarget transactionServiceURL = client.target("http://transaction-management:8083/");
    WebTarget accountServiceURL = client.target("http://user_account_management:8084/");
    WebTarget reportServiceURL = client.target("http://reportmicroservice:8086/");

    /**
     * @author Niels
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("account")
    public Response register(DTUPayUser user) {
        return accountServiceURL.path("account/").request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }


    /**
     * @author Tim
     */
    @DELETE
    @Path("account/{id}")
    public Response deleteAccountById(@PathParam("id") String id) {
        return accountServiceURL.path("account/" + id).request().delete();
    }

    /**
     * @author Lasse
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("transaction")
    public Response addTransaction(TransactionDTO t) throws BadRequestException {
        return transactionServiceURL.path("transaction/").request().post(Entity.entity(t, MediaType.APPLICATION_JSON));
    }

    /**
     * @author Victor
     */
    @GET
    @Path("report/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantTransactions(@PathParam("id") String userId, @QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
        return reportServiceURL.path("report/merchant/"+userId).queryParam("startDate", startDate).queryParam("endDate", endDate).request().get();
    }

}
