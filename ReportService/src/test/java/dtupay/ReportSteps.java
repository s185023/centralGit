package dtupay;

import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messagequeues.Event;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class ReportSteps {
	private ReportManager rm = ReportManager.getInstance();
	private ReportRepository repository = rm.getRepository();
	private List<Transaction> transactionsResult;
	private List<MerchantTransaction> merchantTransactionsResult;
	private String errMessage;
	/**
	 * @author Francisco
	 */
	@After
	public void cleanUp() {
		repository.getAllTransactions().clear();
	}

	/**
	 * @author Francisco
	 */
	@DataTableType
	public Transaction transactionEntry(Map<String, String> entry){
		Transaction t = new Transaction();
		t.setAmount(new BigDecimal(entry.get("amount")));
		t.setDebtor(entry.get("from"));
		t.setCreditor(entry.get("to"));
		t.setTokenID(entry.get("tokenId"));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(entry.get("timestamp"), formatter);
		t.setTimestamp(dateTime);
		t.setDescription(entry.get("description"));
		return t;
	}
	/**
	 * @author Ammad
	 */
	@DataTableType
	public MerchantTransaction merchantTransactionEntry(Map<String, String> entry){
		MerchantTransaction t = new MerchantTransaction();
		t.setAmount(new BigDecimal(entry.get("amount")));
		t.setCreditor(entry.get("receiver"));
		t.setTokenID(entry.get("tokenId"));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(entry.get("timestamp"), formatter);
		t.setTimestamp(dateTime);
		t.setDescription(entry.get("description"));
		return t;
	}
	
	/**
	 * @author Francisco
	 */
	@Given("the following transactions are in the repository:")
	public void theFollowingTransactionsAreInTheRepository(List<Transaction> transactions) throws TimestampParsingException {
		for (Transaction transaction : transactions) 
			repository.addTransaction(transaction);
	}

	/**
	 * @author Ammad
	 */
	@When("the manager asks for all transactions")
	public void theManagerAsksForAllTransactions() {
		this.transactionsResult = rm.getAllTransactions();
	}

	/**
	 * @author Ammad
	 */
	@Then("the manager gets all the following transactions:")
	public void theManagerGetsAllTheFollowingTransactions(List<Transaction> transactions) {
		assertEquals(transactions, transactionsResult);
	}

	/**
	 * @author Francisco
	 */
	@When("the customer {string} asks for all his transactions")
	public void theCustomerAsksForAllHisTransactions(String customerId) throws TimestampParsingException {
		this.transactionsResult = rm.getTransactionsFromCustomer(customerId, null, null);
	}

	/**
	 * @author Francisco
	 */
	@When("the customer {string} asks for all his transactions from {string} to {string}")
	public void theCustomerAsksForAllHisTransactionsFromTo(String id, String fromDate, String toDate) throws TimestampParsingException {
		this.transactionsResult = rm.getTransactionsFromCustomer(id, fromDate, toDate);
	}

	/**
	 * @author Francisco
	 */
	@Then("the customer gets all the following transactions:")
	public void theCustomerGetsAllTheFollowingTransactions(List<Transaction> transactions) {
		assertEquals(transactions, transactionsResult);
	}
	
	/**
	 * @author Francisco
	 */
	@When("the event {string} is received with the following transaction")
	public void theEventIsReceivedWithTheFollowingTransaction(String eventType, List<Transaction> transactions) throws Exception {
		Transaction t = transactions.get(0);
		String[] tArr = {
				t.getTokenID(),
				t.getAmount().toString(),
				t.getCreditor(),
				t.getTimestamp().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
				t.getDebtor(),
				t.getDescription()
		};
	    rm.receiveEvent(new Event(eventType, tArr));
	    transactionsResult = repository.getAllTransactions();
	}
	
	/**
	 * @author Ammad
	 */
	@When("the event {string} is received")
	public void theEventIsReceived(String eventType) throws Exception {
		rm.receiveEvent(new Event(eventType));
	    transactionsResult = repository.getAllTransactions();
	}
	
	/**
	 * @author Ammad
	 */
	@Then("the new transaction is added to the repository:")
	public void theNewTransactionIsAddedToTheRepository(List<Transaction> transactions) {
		assertEquals(transactions, transactionsResult);
	}
	
	/**
	 * @author Francisco
	 */
	@Then("the repository is unchanged:")
	public void theRepositoryIsUnchanged(List<Transaction> transactions) {
		assertEquals(transactions, transactionsResult);
	}
	
	/**
	 * @author Ammad
	 */
	@When("the merchant {string} asks for all his transactions")
	public void theMerchantAsksForAllHisTransactions(String id) throws TimestampParsingException {
		this.merchantTransactionsResult = rm.getTransactionsFromMerchant(id, null, null);
	}
	
	/**
	 * @author Ammad
	 */
	@When("the merchant {string} asks for all his transactions from {string} to {string}")
	public void theMerchantAsksForAllHisTransactionsFromTo(String id, String startDate, String endDate){
		try {
			this.merchantTransactionsResult = rm.getTransactionsFromMerchant(id, startDate, endDate);
		}
		catch(TimestampParsingException e) {
			this.errMessage = e.getMessage();
		}
	}
	
	/**
	 * @author Ammad
	 */
	@Then("the merchant gets all the following transactions:")
	public void theMerchantGetsAllTheFollowingTransactions(List<MerchantTransaction> transactions) {
		assertEquals(transactions, merchantTransactionsResult);
	}
	
	/**
	 * @author Francisco
	 */
	@When("the customer {string} asks for all his transactions from {string}")
	public void theCustomerAsksForAllHisTransactionsFrom(String id, String startDate) throws TimestampParsingException {
		this.transactionsResult = rm.getTransactionsFromCustomer(id, startDate, null);
	}
	
	/**
	 * @author Francisco
	 */
	@When("the customer {string} asks for all his transactions up to {string}")
	public void theCustomerAsksForAllHisTransactionsUpTo(String id, String endDate) throws TimestampParsingException {
		this.transactionsResult = rm.getTransactionsFromCustomer(id, null, endDate);
	}
	
	/**
	 * @author Ammad
	 */
	@When("the merchant {string} asks for all his transactions from {string}")
	public void theMerchantAsksForAllHisTransactionsFrom(String id, String startDate){
		try {
			this.merchantTransactionsResult = rm.getTransactionsFromMerchant(id, startDate, null);
		}
		catch(TimestampParsingException e) {
			this.errMessage = e.getMessage();
		}
	}
	
	/**
	 * @author Ammad
	 */
	@When("the merchant {string} asks for all his transactions up to {string}")
	public void theMerchantAsksForAllHisTransactionsUpTo(String id, String endDate) {
		try {
			this.merchantTransactionsResult = rm.getTransactionsFromMerchant(id, null, endDate);
		}
		catch(TimestampParsingException e) {
			this.errMessage = e.getMessage();
		}
	}
	
	/**
	 * @author Ammad
	 */
	@Then("the merchant gets the error message {string}")
	public void theMerchantGetsTheErrorMessage(String msg) {
		assertEquals(msg, errMessage);
	}
}