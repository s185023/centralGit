Feature: Report behaviour

	# Author: Ammad
  Scenario: GET all transactions succesfully
    Given the following transactions are in the repository:
      | from    | to     | amount | tokenId | timestamp           | description |
      | Franner | Amhe   |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Franner | Joakim |    100 | token2  | 2020-12-30 12:00:58 | description |
      | John    | Tim    |     50 | token3  | 2020-12-30 12:00:58 | description |
      | John    | Joakim |    150 | token4  | 2020-12-30 12:00:58 | description |
    When the manager asks for all transactions
    Then the manager gets all the following transactions:
      | from    | to     | amount | tokenId | timestamp           | description |
      | Franner | Amhe   |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Franner | Joakim |    100 | token2  | 2020-12-30 12:00:58 | description |
      | John    | Tim    |     50 | token3  | 2020-12-30 12:00:58 | description |
      | John    | Joakim |    150 | token4  | 2020-12-30 12:00:58 | description |

	# Author: Francisco
  Scenario: GET customer transactions succesfully
    Given the following transactions are in the repository:
      | from    | to     | amount | tokenId | timestamp           | description |
      | Franner | Amhe   |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Franner | Joakim |    100 | token2  | 2020-12-30 12:00:58 | description |
      | John    | Tim    |     50 | token3  | 2020-12-30 12:00:58 | description |
      | John    | Joakim |    150 | token4  | 2020-12-30 12:00:58 | description |
    When the customer "Franner" asks for all his transactions
    Then the customer gets all the following transactions:
      | from    | to     | amount | tokenId | timestamp           | description |
      | Franner | Amhe   |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Franner | Joakim |    100 | token2  | 2020-12-30 12:00:58 | description |

	# Author: Francisco
  Scenario: GET customer transactions succesfully within month
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim  | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the customer "Franner" asks for all his transactions from "2020-12-01" to "2020-12-31"
    Then the customer gets all the following transactions:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |

	# Author: Francisco
  Scenario: GET customer transactions succesfully from start date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the customer "Franner" asks for all his transactions from "2020-12-31"
    Then the customer gets all the following transactions:
      | from    | to  | amount | tokenId | timestamp           | description |
      | Franner | Tim |     50 | token3  | 2021-01-01 12:00:58 | description |

	# Author: Francisco
  Scenario: GET customer transactions succesfully up to end date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the customer "Franner" asks for all his transactions up to "2020-12-31"
    Then the customer gets all the following transactions:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |

	# Author: Ammad
  Scenario: GET merchant transactions succesfully
    Given the following transactions are in the repository:
      | from    | to     | amount | tokenId | timestamp           | description |
      | Franner | Amhe   |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Franner | Joakim |    100 | token2  | 2020-12-30 12:00:58 | description |
      | John    | Tim    |     50 | token3  | 2020-12-30 12:00:58 | description |
      | John    | Joakim |    150 | token4  | 2020-12-30 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions
    Then the merchant gets all the following transactions:
      | receiver | amount | tokenId | timestamp           | description |
      | Amhe     |    150 | token1  | 2020-12-30 12:00:58 | description |

	# Author: Ammad
  Scenario: GET merchant transactions succesfully within month
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim  | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions from "2020-12-01" to "2020-12-31"
    Then the merchant gets all the following transactions:
      | receiver | amount | tokenId | timestamp           | description |
      | Amhe     |    150 | token1  | 2020-12-30 12:00:58 | description |

	# Author: Ammad
  Scenario: GET merchant transactions succesfully from start date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions from "2020-12-31"
    Then the merchant gets all the following transactions:
      | receiver | amount | tokenId | timestamp           | description |
      | Amhe     |    150 | token4  | 2021-01-01 12:00:58 | description |

	# Author: Ammad
  Scenario: GET merchant transactions succesfully up to end date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions up to "2020-12-31"
    Then the merchant gets all the following transactions:
      | receiver | amount | tokenId | timestamp           | description |
      | Amhe     |    150 | token1  | 2020-12-30 12:00:58 | description |

	# Author: Ammad
  Scenario: The service adds a transaction from an event
    Given the following transactions are in the repository:
      | from   | to      | amount | tokenId | timestamp           | description |
      | Amhe   | Franner |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Amhe   | Franner |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim | Tim     |     50 | token3  | 2021-01-01 12:00:58 | description |
    When the event "PaymentSucceeded" is received with the following transaction
      | from | to      | amount | tokenId | timestamp           | description |
      | Amhe | Franner |    500 | token4  | 2020-12-30 12:00:58 | descriptiin |
    Then the new transaction is added to the repository:
      | from   | to      | amount | tokenId | timestamp           | description |
      | Amhe   | Franner |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Amhe   | Franner |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim | Tim     |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Amhe   | Franner |    500 | token4  | 2020-12-30 12:00:58 | descriptiin |

	# Author: Francisco
  Scenario: The service does not add a transaction from an event
    Given the following transactions are in the repository:
      | from   | to      | amount | tokenId | timestamp           | description |
      | Amhe   | Franner |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Amhe   | Franner |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim | Tim     |     50 | token3  | 2021-01-01 12:00:58 | description |
    When the event "PaymentNotSucceeded" is received
    Then the repository is unchanged:
      | from   | to      | amount | tokenId | timestamp           | description |
      | Amhe   | Franner |    150 | token1  | 2020-12-30 12:00:58 | description |
      | Amhe   | Franner |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Joakim | Tim     |     50 | token3  | 2021-01-01 12:00:58 | description |

	# Author: Ammad
  Scenario: GET merchant transactions unsuccesfully due to failed parsing on start date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions from "wrong format"
    Then the merchant gets the error message "Wrong format"

	# Author: Francisco      
  Scenario: GET merchant transactions unsuccesfully due to failed parsing on one date
    Given the following transactions are in the repository:
      | from    | to   | amount | tokenId | timestamp           | description |
      | Franner | Amhe |    150 | token1  | 2020-12-30 12:00:58 | description |
      | John    | Jane |    100 | token2  | 2021-01-01 12:00:58 | description |
      | Franner | Tim  |     50 | token3  | 2021-01-01 12:00:58 | description |
      | Joakim  | Amhe |    150 | token4  | 2021-01-01 12:00:58 | description |
    When the merchant "Amhe" asks for all his transactions from "2020-12-01" to "wrong format"
    Then the merchant gets the error message "Wrong format"
