package messagequeues;

public interface EventSender {

	void sendEvent(Event event) throws Exception;

}
