package dtupay;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messagequeues.RabbitMqListener;

/**
 * @author Francisco
 * 
 */
@QuarkusMain
public class Main {
    public static void main(String... args) {
        Quarkus.run(MyApp.class, args);
    }

    public static class MyApp implements QuarkusApplication {

        @Override
        public int run(String... args) throws Exception {
            startUp();
            Quarkus.waitForExit();
            return 0;
        }

        private void startUp() throws Exception {
            ReportManager service = ReportManager.getInstance();
            new RabbitMqListener(service).listen();
        }

    }
}