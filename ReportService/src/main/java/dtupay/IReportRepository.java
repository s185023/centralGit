package dtupay;

import java.util.List;
/**
 * @author Francisco
 */

public interface IReportRepository {
	List<Transaction> getAllTransactions();
	
	void addTransaction(Transaction t);
}
