package dtupay;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ammad
 */
public class ReportRepository implements IReportRepository {
	
	List<Transaction> transactions = new ArrayList<Transaction>();

	static ReportRepository repo = null;


	// Singleton instance
	public static ReportRepository getInstance() {
		if(repo == null) {
			repo = new ReportRepository();
		}
		return repo;
	}
		
	@Override
	public List<Transaction> getAllTransactions() {
		return transactions;
	}

	@Override
	public void addTransaction(Transaction t) {
		transactions.add(t);
	}
}
