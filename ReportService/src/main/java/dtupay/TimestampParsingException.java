package dtupay;

/**
 * @author Ammad
 */
public class TimestampParsingException extends Exception{
    public TimestampParsingException(String errorMessage) {
        super(errorMessage);
    }

}
