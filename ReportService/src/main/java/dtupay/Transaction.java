package dtupay;

import java.math.BigDecimal;
import java.time.LocalDateTime;
/**
 * @author Francisco
 */
public class Transaction {
    private String debtor;
    private String creditor;
    private BigDecimal amount;
    private LocalDateTime timestamp;
    private String tokenID;
    private String description;

    public Transaction() {
    }

    public Transaction(String debtor, String creditor, BigDecimal amount, LocalDateTime timestamp, String tokenID,
            String description) {
        setDebtor(debtor);
        setCreditor(creditor);
        setAmount(amount);
        setTimestamp(timestamp);
        setTokenID(tokenID);
        setDescription(description);
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o instanceof Transaction) {
            Transaction that = (Transaction) o;
            return this.getTokenID().equals(that.getTokenID())
					&& this.getAmount().equals(that.getAmount())
					&& this.getCreditor().equals(that.getCreditor())
					&& this.getDebtor().equals(that.getDebtor())
					&& this.getTimestamp().equals(that.getTimestamp())
					&& this.getDescription().equals(that.getDescription());
        } else {
            return false;
        }
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public String getDebtor() {
        return debtor;
    }

    public void setDebtor(String debtor) {
        this.debtor = debtor;
    }

    public String getCreditor() {
        return creditor;
    }

    public void setCreditor(String creditor) {
        this.creditor = creditor;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

}
