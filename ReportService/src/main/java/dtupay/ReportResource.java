package dtupay;

import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/report")
public class ReportResource {

	ReportManager rm = ReportManager.getInstance();

	/**
	 * @author Francisco
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transaction> getAllTransactions() {
		return rm.getAllTransactions();
	}

	/**
	 * @author Francisco
	 */
	@GET
	@Path("customer/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Transaction> getCustomerTransactions(@PathParam("id") String userId,
			@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
		try {
			return rm.getTransactionsFromCustomer(userId, startDate, endDate);
		} catch (TimestampParsingException e) {
			e.printStackTrace();
			throw new BadRequestException(e.getMessage());
		}
	}

	/**
	 * @author Ammad
	 */
	@GET
	@Path("merchant/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MerchantTransaction> getMerchantTransactions(@PathParam("id") String userId,
			@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate) {
		try {
			return rm.getTransactionsFromMerchant(userId, startDate, endDate);

		} catch (TimestampParsingException e) {
			e.printStackTrace();
			throw new BadRequestException(e.getMessage());
		}
	}
}
