package dtupay;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

import messagequeues.Event;
import messagequeues.EventReceiver;
import messagequeues.EventSender;
import messagequeues.RabbitMqSender;

public class ReportManager implements EventReceiver {

	ReportRepository repository;
	EventSender sender;
	static ReportManager reportManager = null;


	public ReportManager(EventSender eventSender, ReportRepository repository) {
		this.sender = eventSender;
		this.repository = repository;
	}


	/**
	 * @author Francisco
	 * 
	 */
	public static ReportManager getInstance() {
		if(reportManager == null) {
			reportManager = new ReportManager(new RabbitMqSender(), ReportRepository.getInstance());
		}
		return reportManager;
	}

	/**
	 * @author Francisco
	 */
	public ReportRepository getRepository() {
		return repository;
	}


	/**
	 * @author Francisco
	 */
	public List<Transaction> getAllTransactions() {
		return repository.getAllTransactions();
	}

	/**
	 * @author Ammad
	 */
	@Override
	public void receiveEvent(Event event) throws Exception {
		if (event.getEventType().equals("PaymentSucceeded")) {
			String[] transactionsParameters = event.getArguments();

			Transaction transaction = new Transaction();
			transaction.setTokenID(transactionsParameters[0]);
			transaction.setAmount(new BigDecimal(transactionsParameters[1]));
			transaction.setCreditor(transactionsParameters[2]);

			LocalDateTime dateTime = parseStringToLocalDateTime(transactionsParameters[3], DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			
			transaction.setTimestamp(dateTime);
			transaction.setDebtor(transactionsParameters[4]);
			transaction.setDescription(transactionsParameters[5]);
			repository.addTransaction(transaction);
		}
	}

	/**
	 * @author Ammad
	 */
	private LocalDateTime parseStringToLocalDateTime(String str, DateTimeFormatter dateTimeFormatter) throws TimestampParsingException {
		LocalDateTime dateTime;
		
		try {
			if(dateTimeFormatter == DateTimeFormatter.ISO_LOCAL_DATE) {
				LocalDate d = LocalDate.parse(str, dateTimeFormatter);
				dateTime = d.atStartOfDay();
			}
			else {
				dateTime = LocalDateTime.parse(str, dateTimeFormatter);
			}
		}
		catch(Exception e) {
			throw new TimestampParsingException("Wrong format");
		}


		return dateTime;
	}

	/**
	 * @author Francisco
	 */
	public List<Transaction> getTransactionsFromCustomer(String userId, String startDate, String endDate) throws TimestampParsingException {
		var s = repository.getAllTransactions().stream()
				.filter(t -> t.getDebtor().equals(userId));
		
		s = filterTransactionsFromTime(startDate, endDate, s);
		return s.collect(toList());
	}
	
	/**
	 * @author Ammad
	 */
	public List<MerchantTransaction> getTransactionsFromMerchant(String userId, String startDate, String endDate) throws TimestampParsingException {
		var s = repository.getAllTransactions().stream()
				.filter(t -> t.getCreditor().equals(userId));
		
		s = filterTransactionsFromTime(startDate, endDate, s);
		return s.map(t -> new MerchantTransaction(t)).collect(toList());
	}


	/**
	 * @author Francisco
	 */
	private Stream<Transaction> filterTransactionsFromTime(String startDate, String endDate, Stream<Transaction> s) throws TimestampParsingException {		
		if (startDate != null) {
			LocalDateTime startD = parseStringToLocalDateTime(startDate, DateTimeFormatter.ISO_LOCAL_DATE);
			s = s.filter(t -> t.getTimestamp().isAfter(startD));
		}
		if (endDate != null) {
			LocalDateTime endD = parseStringToLocalDateTime(endDate, DateTimeFormatter.ISO_LOCAL_DATE);
			s = s.filter(t -> t.getTimestamp().isBefore(endD));
		}
		return s;
	}

}
