package dtupay;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;

import java.util.concurrent.CompletableFuture;

import dtu.ws.fastmoney.*;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import messagequeues.Event;
import messagequeues.EventSender;
import messagequeues.RabbitMqSender;

public class MoneyTransferSteps {

	private BankService bank = new BankServiceService().getBankServicePort();

	private Event event;
	private String accountIDC;
	private String accountIDM;

	EventSender sender = new RabbitMqSender();
	MoneyTransfer mt = new MoneyTransfer(sender);
	String error;

	CompletableFuture<Void> result = new CompletableFuture<>();
	CompletableFuture<Boolean> mutex = new CompletableFuture<>(); // Make sure we dont run into race conditions


	public MoneyTransferSteps() {
		mt.setSender(e -> {
			event = e;
			mutex.complete(true);
		});
	}

	/**
	 * @author Victor
	 */
	@Given("the customer {string} {string} with CPR {string} has a bank account And the balance of that account is {int}")
	public void theCustomerWithCPRHasABankAccountAndTheBalanceOfThatAccountIs(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
		User c = new User();
		c.setCprNumber(cpr);
		c.setFirstName(firstName);
		c.setLastName(lastName);

		accountIDC = bank.createAccountWithBalance(c, new BigDecimal(balance));
	}

	/**
	 * @author Niels
	 */
	@Given("the merchant {string} {string} with CPR number {string} has a bank account And the balance of that account is {int}")
	public void theMerchantWithCPRNumberHasABankAccountAndTheBalanceOfThatAccountIs(String firstName, String lastName, String cpr, int balance) throws BankServiceException_Exception {
		User m = new User();
		m.setCprNumber(cpr);
		m.setFirstName(firstName);
		m.setLastName(lastName);


		accountIDM = bank.createAccountWithBalance(m, new BigDecimal(balance));
	}

	/**
	 * @author Lasse
	 */
	@When("I have received the event {string}")
	public void iHaveReceivedTheEvent(String string) throws Exception {
		String[] arr = { "", "1000", accountIDM, "", accountIDC, "a description" };

		new Thread( () -> {
			try {
				mt.receiveEvent(new Event(string, arr));
				result.complete(null);
			} catch (Exception e) {
				error = e.getMessage();
				result.complete(null);
			}
		}).start();

		mutex.join();
	}

	/**
	 * @author Niels
	 */
	@Then("the service sends an event {string}")
	public void theServiceSendsAnEvent(String string) {
		assertEquals(string, event.getEventType());
	}

	/**
	 * @author Victor
	 */
	@When("I have received the event {string} with an unknown merchantId {string}")
	public void iHaveReceivedTheEventWithAnUnknownMerchantId(String eventType, String merchant) throws Exception {
		String[] arr = { "", "1000", merchant, "", accountIDC, "a description" };

		new Thread( () -> {
			try {
				mt.receiveEvent(new Event(eventType, arr));
				result.complete(null);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				error = e.getMessage();
				result.complete(null);
			}
		}).start();

		mutex.join();
	}

	/**
	 * @author Niels
	 */
	@Given("I receive the event {string}")
	public void iReceiveTheEvent(String eventType) {
		try {
			mt.receiveEvent(new Event(eventType, new String[] {"0", "1000", "2", "3", "4", "a description", accountIDC, accountIDM}));
			result.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Niels
	 */
	@Given("I receive the event {string} with an unknown merchantId")
	public void iReceiveTheEventWithAnUnknownMerchantId(String eventType) {
		try {
			mt.receiveEvent(new Event(eventType, new String[] {"0", "1000", "2", "3", "4", "a description", accountIDC, "Missing"}));
			result.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Lasse
	 */
	@Given("I receive the relevant event {string}")
	@Given("I receive the irrelevant event {string}")
	public void i_receive_the_irrelevant_event(String eventType) {
		try {
			mt.receiveEvent(new Event(eventType, new String[] {"0", "1", "2", "3", "4", "5"}));
		} catch (Exception e) {
			error = e.getMessage();
		}
	}

	/**
	 * @author Lasse
	 */
	@Then("I send no events")
	public void i_send_no_events() {
		assertNull(event);
	}


	/**
	 * @author Victor
	 */
	@After
	public void afterScenario() {
		try {
			if(accountIDC != null) {
				bank.retireAccount(accountIDC);
			}

			if(accountIDM != null) {
				bank.retireAccount(accountIDM);
			}


		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
		}
	}


}
