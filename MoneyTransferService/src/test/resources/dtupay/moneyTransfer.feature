Feature: MoneyTransfer

  # Author: Niels
  Scenario: Successful transfer
    Given the customer "Sebastian" "Sørensen" with CPR "270497-3805" has a bank account And the balance of that account is 1000
    And the merchant "Emilie" "Laursen" with CPR number "211274-4676" has a bank account And the balance of that account is 2000
  	When I have received the event "TokenValid"
    Given I receive the event "BankAccountNumbers"
    Then the service sends an event "PaymentSucceeded"

  # Author: Victor
  Scenario: Unsuccessful transfer
    Given the customer "Sebastian" "Sørensen" with CPR "270497-3805" has a bank account And the balance of that account is 1000
    And the merchant "Emilie" "Laursen" with CPR number "211274-4676" has a bank account And the balance of that account is 2000
  	When I have received the event "TokenValid" with an unknown merchantId "Unknown"
    Given I receive the event "BankAccountNumbers" with an unknown merchantId
    Then the service sends an event "PaymentNotSucceeded"

  # Author: Victor
  Scenario: Unsuccessful transfer: Merchant in not a DTUPay user
    Given I receive the relevant event "MerchantNotRegistered"
    Then the service sends an event "PaymentNotSucceeded"

  # Author: Lasse
  Scenario: Invalid token
    Given the customer "Sebastian" "Sørensen" with CPR "270497-3805" has a bank account And the balance of that account is 1000
    And the merchant "Emilie" "Laursen" with CPR number "211274-4676" has a bank account And the balance of that account is 2000
    When I have received the event "TokenNotValid"
    Then the service sends an event "PaymentNotSucceeded"

  # Author: Lasse
  Scenario: Receive an irrelevant event
    Given I receive the irrelevant event "PaymentSucceeded"
    Then I send no events