package dtupay;

import java.math.BigDecimal;

import java.util.Arrays;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messagequeues.Event;
import messagequeues.EventReceiver;
import messagequeues.EventSender;

public class MoneyTransfer implements EventReceiver {
	BankService bank = new BankServiceService().getBankServicePort();
	private EventSender eventSender;

	public MoneyTransfer(EventSender sender) {
		this.eventSender = sender;
	}

	public void setSender(EventSender sender) {
		this.eventSender = sender;
	}

	/**
	 * @author Niels
	 */
	@Override
	public void receiveEvent(Event event) throws Exception {
		switch (event.getEventType()) {
			case "TokenValid":
				String[] transactionParameters = event.getArguments();

				String token = transactionParameters[0];
				String amount = transactionParameters[1];
				String merchantId = transactionParameters[2];
				String timeStamp = transactionParameters[3];
				String customerId = transactionParameters[4];
				String description = transactionParameters[5];

				Event getBankAccountNumbers = new Event("GetBankAccountNumbers", new String[]{token, amount, merchantId, timeStamp, customerId, description, "", ""});
				eventSender.sendEvent(getBankAccountNumbers);

				break;
			case "BankAccountNumbers":
				try {
					String[] BankAccountsNumbers = event.getArguments();
					bank.transferMoneyFromTo(BankAccountsNumbers[6], BankAccountsNumbers[7], new BigDecimal(BankAccountsNumbers[1]), BankAccountsNumbers[5]);
					Event e = new Event("PaymentSucceeded", Arrays.copyOf(BankAccountsNumbers, BankAccountsNumbers.length - 2));
					eventSender.sendEvent(e);
				} catch (Exception ex) {
					Event e = new Event("PaymentNotSucceeded");
					eventSender.sendEvent(e);
				}

				break;
			case "TokenNotValid":
				Event e = new Event("PaymentNotSucceeded");
				eventSender.sendEvent(e);
				break;

			case "MerchantNotRegistered":
				Event ev = new Event("PaymentNotSucceeded");
				eventSender.sendEvent(ev);
				break;
		}
	}
}
