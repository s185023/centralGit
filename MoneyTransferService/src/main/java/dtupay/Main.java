package dtupay;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messagequeues.EventSender;
import messagequeues.RabbitMqListener;
import messagequeues.RabbitMqSender;

@QuarkusMain
public class Main {
    public static void main(String... args) {
        Quarkus.run(MyApp.class, args);
    }

    public static class MyApp implements QuarkusApplication {

        @Override
        public int run(String... args) throws Exception {
            startUp();
            Quarkus.waitForExit();
            return 0;
        }

        private void startUp() throws Exception {
            EventSender s = new RabbitMqSender();
            MoneyTransfer service = new MoneyTransfer(s);
            new RabbitMqListener(service).listen();
        }

    }
}