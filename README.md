# Exam Project
#### Software Development of Web Services, Fall 20
### Group 7
* Victor Foged, s174267
* Ammad Hameed, s174297
* Tim Martin Jensen, s185027
* Lasse Steen Pedersen, s174253
* Niels With Mikkelsen, s174290
* Joakim Bøegh Levorsen, s185023
* Francisco Jose Torres Mendez, s175406

## Folders and their Content
Our project is divided into eight sub-folders, these sub-folders are explained below:

* `/client/`: Our client containing our end-to-end tests.
* `/MoneyTransferService/`: Our money transfer microservice and service tests.
* `/ReportService/`: Our report microservice and service tests.
* `/rest-facade/`: Our REST-facade.
* `/swagger-files/`: Folder containing our swagger files for both our internal and external endpoints.
* `/token-management/`: Our token management microservice and service tests.
* `/TransactionMicroservice/`: Our transaction microservice and service tests.
* `/userAccountManagement/`: Our account management microservice and service tests.



## Services and their Ports

| Service               | Port |
| --------------------- | ---- |
| REST Facade           | 8081 |
| Tokens                | 8082 |
| Transactions          | 8083 |
| UserAccountManagement | 8084 |
| MoneyTransfer         | 8085 |
| Reports               | 8086 |
