package dtupay;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messagequeues.EventSender;
import messagequeues.Event;

public class TransactionSteps {
	TransactionManager tm;
	Event event;
	CompletableFuture<Boolean> result = new CompletableFuture<>();

	CompletableFuture<Boolean> mutex = new CompletableFuture<>(); // Make sure we dont run into race conditions
	private Transaction transaction;
	/**
	 * @author Francisco
	 */
	public TransactionSteps() {
		tm = new TransactionManager(new EventSender() {
			@Override
			public void sendEvent(Event e) throws Exception {
				event = e;
				mutex.complete(true);
			}
		});
	}

	/**
	 * @author Francisco
	 */
	@When("a merchant initiates a transaction with the token {string} and amount {int}")
	public void aMerchantInitiatesATransactionWithTheTokenAndAmount(String token, Integer amount) {
		transaction = new Transaction();

		transaction.setAmount(new BigDecimal(amount));
		transaction.setCreditor("TestCreditor");
		transaction.setTokenID(token);
	}
	
	/**
	 * @author Ammad
	 */
	@Then("I sent the event {string}")
	public void iSentTheEvent(String ev) {
		new Thread(() -> {
			try {
				result.complete(tm.makeTransaction(transaction));
			} catch (Exception e) {
				result.complete(false);
			}
		}).start();
		
		mutex.join();
		
		assertEquals(ev, event.getEventType());
	}

	/**
	 * @author Ammad
	 */
	@Given("I receive the event {string}")
	@Given("I receive the irrelevant event {string}")
	public void iReceiveTheEvent(String ev) {
		try {
			tm.receiveEvent(new Event(ev));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author Francisco
	 */
	@Then("the transaction is successful")
	public void theTransactionIsSuccessful() {
		assertTrue(result.join());	
	}
	
	/**
	 * @author Ammad
	 */
	@Then("the transaction is not successful")
	public void theTransactionIsNotSuccessful() {
		assertFalse(result.join());	

	}

	/**
	 * @author Francisco
	 */
	@Then("I ignore the event")
	public void iIgnoreTheEvent() {
		assertNull(event);
	}

}
