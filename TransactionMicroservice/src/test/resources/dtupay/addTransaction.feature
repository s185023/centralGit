Feature: Transaction

  # Author: Francisco
  Scenario: Successful Transaction
    When a merchant initiates a transaction with the token "tokenid" and amount 1000
    Then I sent the event "TokenValidation"
    Given I receive the event "PaymentSucceeded"
    Then the transaction is successful

  # Author: Ammad
  Scenario: Unsuccessful Transaction
    When a merchant initiates a transaction with the token "tokenid" and amount 1000
    Then I sent the event "TokenValidation"
    Given I receive the event "PaymentNotSucceeded"
    Then the transaction is not successful
    
  # Author: Francisco
  Scenario: Received uninteresting event
  	Given I receive the irrelevant event "UserVerified"
  	Then I ignore the event
