package dtupay;

import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import messagequeues.Event;
import messagequeues.EventReceiver;
import messagequeues.EventSender;

public class TransactionManager implements EventReceiver {
	private EventSender eventSender;
	private CompletableFuture<Boolean> result;

	public TransactionManager(EventSender b) {
		this.eventSender = b;
	}

	/**
	 * @author Francisco
	 */
	public boolean makeTransaction(Transaction t) throws Exception {

		t.setTimestamp(LocalDateTime.now());
		String[] transactionParameters = { t.getTokenID(), t.getAmount().toString(), t.getCreditor(), t.getTimestamp().toString(), "", t.getDescription() };
		Event event = new Event("TokenValidation", transactionParameters);
		result = new CompletableFuture<>();
		eventSender.sendEvent(event);
		
		return result.join();
	}

	/**
	 * @author Ammad
	 */
	@Override
	public void receiveEvent(Event event) {
		if (event.getEventType().equals("PaymentSucceeded")) {
			result.complete(true);
		}
		else if(event.getEventType().equals("PaymentNotSucceeded")) {
			result.complete(false);
		}
	}

}
