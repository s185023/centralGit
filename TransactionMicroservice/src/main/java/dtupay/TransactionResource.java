package dtupay;

import java.net.URI;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Lasse
 */

@Path("/transaction")
public class TransactionResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTransaction(Transaction t) throws BadRequestException {
		TransactionManager tm = new TransactionManagerFactory().getService();
		try {
			if (tm.makeTransaction(t)) {
				return Response.created(new URI("http://localhost:8083/transaction")).build();
			} else {
				return Response.status(Response.Status.BAD_REQUEST).entity("payment not successful").build();
			}
		} catch (Exception e) {
			throw new BadRequestException(e.getMessage());
		}
	}

}
