package dtupay;

import messagequeues.EventSender;
import messagequeues.RabbitMqListener;
import messagequeues.RabbitMqSender;
/**
 * @author Ammad
 */
public class TransactionManagerFactory {
	static TransactionManager service = null;

	public TransactionManager getService() {
		if (service != null) {
			return service;
		}

		EventSender b = new RabbitMqSender();
		service = new TransactionManager(b);
		RabbitMqListener r = new RabbitMqListener(service);
		try {
			r.listen();
		} catch (Exception e) {
			throw new Error(e);
		}
		return service;
	}
}
